/**
 * The actual physics methods.
 * This is going to be super simple. All methods expect sufficient data to be provided; no InsufficientDataExceptions
 * will be thrown here.
 * This is because CircuitLib should be simple to prevent any physics errors. Our Circuit.java class can be complex if it
 * means keeping this simple.
 * <p>
 * It's better for our program to crash and burn than to give the user the wrong numbers and
 * tell him/her to "trust us" and "it'll be OK".
 */
class CircuitLib {

    /**
     * Find the equivalent capacitance of capacitors in series.
     *
     * @param capacitors An array of capacitances (in series)
     * @return the reciprocal of the sum of the reciprocals of all C's in capacitors
     * (1/(1/C1+1/C2+1/C3...))
     */
    public static double cEqSeries(double[] capacitors) {
        double sum = 0.0;
        for (double capacitance : capacitors) {
            sum += 1.0 / capacitance;
        }
        return 1.0 / sum;
    }

    /**
     * Find a missing capacitance for a series circuit, given the known capacitors and a CEq.
     * See doc comment for cEqSeries for formula for CEq.
     * Essentially solve for X: 1/Ceq = 1/C1+1/C2+1/C3+...+1/X, given all capacitances except X.
     *
     * @param knownCapacitances: The list of known capacitors known. Excludes the missing capacitor
     * @param CEq:               The equivalent capacitance.
     * @return The value of the missing capacitance that makes the equivalent capacitance equal to CEq.
     */
    public static double missingCapacitanceSeries(double[] knownCapacitances, double CEq) {
        double recipCapacitance; // the reciprocal of the missing capacitance.
        recipCapacitance = 1.0 / CEq; //temporary
        for (double capacitance : knownCapacitances) {
            recipCapacitance -= 1 / capacitance;
        }

        return 1.0 / recipCapacitance;
    }

    /**
     * Find the equivalent capacitance of capacitors in series.
     *
     * @param capacitors An array of capacitances (in series)
     * @return the sum of the capacitors.
     */
    public static double cEqSimpleParallel(double[] capacitors) {
        double sum = 0.0;
        for (double capacitance : capacitors) {
            sum += capacitance;
        }
        return sum;
    }

    /**
     * Find a missing capacitance for a series circuit, given the known capacitors and a cEq.
     * cEq of parallel capacitors is the sum of their capacitances.
     * Essentially solve for X: cEq = C1 + C2 + X + C3 ... + CN, given all capacitances except X.
     *
     * @param capacitances: The list of known capacitors known. Excludes the missing capacitor
     * @param cEq:          The equivalent capacitance.
     * @return The value of the missing capacitance that makes the equivalent capacitance equal to CEq.
     */
    public static double missingCapacitanceSimpleParallel(double[] capacitances, double cEq) {
        double missingCapacitance = cEq;
        for (double capacitance : capacitances) {
            missingCapacitance -= capacitance;
        }
        return missingCapacitance;
    }
}

