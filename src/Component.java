/**
 * A re-usable class for components (capacitors, resistors, etc).
 * For version 1 think of this as a really bloated class for capacitors.
 * For version 2, this *might* include resistors (unlikely).
 */
class Component {
    private final String name;
    private final String componentType;
    private double value;

    /**
     * Constructor for a component object.
     *
     * @param name          A variable name for a component. useful for keepring track of components in complex circuits.
     * @param componentType The type of component. Can be "Battery" or "Capacitor" as of right now.
     * @param value         The value of the component (capacitance for capacitors, electric potentnial for batteries).
     *                      Does not include units.
     */
    Component(String name, String componentType, double value) {
        this.name = name;
        this.componentType = componentType;
        this.value = value;
    }

    /**
     * Getter for this.name
     *
     * @return the name of the component.
     */
    String getName() {
        return name;
    }

    /**
     * Getter for component type. Components can currently be capacitors or power supplies.
     *
     * @return the type of component.
     */
    String getComponentType() {
        return this.componentType;
    }

    /**
     * Getter for component value. Can be capacitance or electric potential (voltage).
     * Does not include units; that's up to the circuit.
     *
     * @return the component value.
     */
    double getValue() {
        return value;
    }

    /**
     * Setter for component value. Does not include units.
     *
     * @param newValue the value to set.
     */
    void setValue(double newValue) {
        this.value = newValue;
    }

    /**
     * Equality testing for circuit components.
     *
     * @param other the other circuit component.
     * @return true if this component is identical to other, false otherwise.
     */
    boolean circuitEquals(Component other) {
        return (
                this.getValue() == other.getValue() && this.getName().equals(other.getName()) && this.getComponentType().equals(other.getComponentType())
        );
    }

    /**
     * String representation of a component. Necessary for printing circuits to standard output.
     *
     * @return A string representation of the component. This should be a valid component line in a circuit file.
     */
    @Override
    public String toString() {
        String[] data = {this.getComponentType(), this.getName(), String.valueOf(this.getValue())};
        return String.join(" ", data);
    }
}
