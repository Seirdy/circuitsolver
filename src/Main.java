/**
 * Turn an unfinished circuit file into a solved circuit file!
 */
class Main {

    /*To reset your project to the online version: type this:*/
    /*cd .. && rm -rf circuitsolver && git clone https://gitlab.com/Seirdy/circuitsolver && cd circuitsolver*/

    /**
     * Turn standard input into a solved circuit file!
     *
     * @param args command-line args
     * @throws InsufficientDataException if the circuit is unsolvable because it has too many unknowns.
     */
    public static void main(String[] args) throws InsufficientDataException {
        // parse input.
        String[] stdin = StdIn.readAllLines();
        Circuit solvedCircuit = Parsing.buildCircuitFromLines(stdin);
        solvedCircuit.justSolveTheWholeCircuitAlready();
        StdOut.println(solvedCircuit.toString());
        // yay ur done noe xd
    }
}
