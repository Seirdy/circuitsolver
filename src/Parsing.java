import java.util.ArrayList;
import java.util.Arrays;

/**
 * Library for parsing input strings.
 * Main.main() reads standard input with StdIn.readAllLines().
 * This library contains utility methods to turn that array of
 * strings into a circuit object.
 */
class Parsing {


    /**
     * Turn an input file into a 2D array of Strings.
     * StdIn.readAllLines() turns stdin into a 1D array of Strings,
     * in which each line of stdin is an element of that array.
     * This method splits each of those elements by word.
     *
     * @param lines: 1D array of Strings to be split by word. String[lines]
     * @return 2D array of words. String[lines][words].
     */
    static String[][] parseCircuit(String[] lines) {
        String[][] parsed = new String[lines.length][];
        for (int i = 0; i < lines.length - 1; i++) {
            String line = lines[i];
            String[] wordsToAdd = line.split("\\s+");
            parsed[i] = wordsToAdd;
        }
        // do the last line manually because CEq line has two elements
        parsed[parsed.length - 1] = lines[lines.length - 1].split("\\s");
        return parsed;
    }

    private static String[][][] splitBranches(String[][] parsedCircuit) {
        // branches are segments of a circuit file separated by hyphens.
        // get all indices of hyphens.
        ArrayList<Integer> indicesList = new ArrayList<>();
        for (int i = 0; i < parsedCircuit.length; i++) {
            if (parsedCircuit[i][0].equals("-")) {
                indicesList.add(i);
            }
        }
        // convert ArrayList<Integer> to int[]
        int[] indices = indicesList.stream().mapToInt(i -> i).toArray();
        // split parsedCircuit by indices of hyphens.
        // Num of branches = num of hyphens - 1
        String[][][] parsedBranches = new String[indices.length - 1][][];
        // Add branches to parsedBranches
        for (int metaIndex = 0; metaIndex < indices.length - 1; metaIndex++) {
            int from = indices[metaIndex] + 1; // `from` param is inclusive, and we don't want to include the hyphen.
            int to = indices[metaIndex + 1]; // `to` param is exclusive.
            String[][] branch = Arrays.copyOfRange(parsedCircuit, from, to);
            parsedBranches[metaIndex] = branch;
        }
        return parsedBranches;
    }

    /**
     * Parse a 2d array of components from a single circuit branch.
     *
     * @param unparsedComponents: A subset of the output of parseCircuit that just concerns one type of component.
     * @return A list of Component objects.
     */
    static Component[] parseComponents(String[][] unparsedComponents) {
        Component[] parsedComponents = new Component[unparsedComponents.length];
        for (int i = 0; i < unparsedComponents.length; i++) {
            String[] line = unparsedComponents[i];
            parsedComponents[i] = new Component(line[1], line[0], Double.parseDouble(line[2]));
        }
        return parsedComponents;
    }

    private static Component[][] getComponentsFromSplitBranches(String[][][] splittedBranches) {
        Component[][] capacitors = new Component[splittedBranches.length][];
        for (int i = 0; i < capacitors.length; i++) {
            String[][] unparsedComponents = Arrays.copyOfRange(
                    splittedBranches[i], 0, splittedBranches[i].length - 1 // chop off cEqBranch
            );
            capacitors[i] = parseComponents(unparsedComponents);
        }
        return capacitors;
    }

    private static double[] getCEqBranchesFromSplittedBranches(String[][][] splittedBranches) {
        double[] cEqBranches = new double[splittedBranches.length];
        for (int i = 0; i < cEqBranches.length; i++) {
            String[][] branch = splittedBranches[i];
            String cEqBranchString = branch[branch.length - 1][1];
            cEqBranches[i] = Double.parseDouble(cEqBranchString);
        }
        return cEqBranches;
    }

    /**
     * Turn output of StdIn.readAllLines() into a circuit object
     *
     * @param stdin the circuit file split into an array by newlines.
     * @return a circuit object from the circuit file.
     */
    static Circuit buildCircuitFromLines(String[] stdin) {
        String[][] input = Parsing.parseCircuit(stdin);
        // split input by category.
        String[] unitLine = input[0];
        String[] batteryLine = input[1];
        String[][] capacitorLines = Arrays.copyOfRange(
                input, 2, input.length - 1
        ); // The segment of input that concerns capacitors.
        String[] cEqCircuitLine = input[input.length - 1];

        String[] units = Arrays.copyOfRange(
                unitLine, 1, 3
        );
        double cEqCircuit = Double.parseDouble(cEqCircuitLine[1]);
        Component battery = new Component(batteryLine[1], batteryLine[0], Double.parseDouble(batteryLine[2]));
        String[][][] splittedBranches = splitBranches(capacitorLines);
        Component[][] capacitors = getComponentsFromSplitBranches(splittedBranches);
        double[] cEqBranches = getCEqBranchesFromSplittedBranches(splittedBranches);
        return new Circuit(capacitors, battery, units, cEqCircuit, cEqBranches);
    }
}
