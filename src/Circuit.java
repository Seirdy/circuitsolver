import java.util.Arrays;

/**
 * Represents a circuit.
 * A circuit has a list of capacitors, a power supply, equivalent (total) capacitance, and units of measurement.
 * Up to one capacitor's capacitance OR the equivalent capacitance can be unknown. All missing values
 * are calcualted in getters using algorithms in CircuitLib.
 * Units of measurement will be more useful in CircuitViewer (V2).
 */
class Circuit {
    private final String[] units;
    private Component[][] capacitors;
    /* private Component[] resistors;*/ // VERSION 2 possibly, or never :)
    private Component powerSupply;
    private double cEqCircuit;
    private double[] cEqBranches;

    /**
     * Circuit constructor with its components and information.
     *
     * @param capacitors  All the capacitors to put in the circuit.
     *                    Up to one capacitor can have unknown capacitance (capacitor.value = -1).
     * @param powerSupply the emf/battery/generator/potato.
     * @param units       A 1D array of size 2 containing units for
     *                    electric potential (eg volt or V)
     *                    and capacitance (eg microfarad or uF).
     * @param cEqCircuit  the equivalent capacitance of the circuit. Can be unknown (-1) if all capacitances are known.
     * @param cEqBranches The equivalent capacitance for each branch of the parallel circuit.
     */
    Circuit(Component[][] capacitors, Component powerSupply, String[] units, double cEqCircuit, double[] cEqBranches) {
        this.capacitors = capacitors;
        this.powerSupply = powerSupply;
        this.cEqCircuit = cEqCircuit;
        this.units = units;
        this.cEqBranches = cEqBranches;
    }

    /**
     * Count the number of missing capacitors.
     * Useful when determining if the circuit is solvable (See InsufficientDataException).
     *
     * @return the number of missing capacitances in the circuit.
     */
    private static int countMissing(double[] capacitances) {
        int count = 0;
        for (double capacitance : capacitances) {
            if (capacitance == -1) {
                count++;
            }
        }
        return count;
    }

    private boolean cEqBranchesFound() {
        for (double cEqBranch : this.cEqBranches) {
            if (!(cEqBranch >= 0)) {
                return false;
            }
        }
        return true;
    }

    private boolean cEqCircuitFound() {
        if (this.cEqCircuit < 0) {
            if (cEqBranchesFound()) {
                this.cEqCircuit = CircuitLib.cEqSimpleParallel(this.cEqBranches); // 1BA
                return true;
            }
        }
        return !(this.cEqCircuit < 0);
    }

    private double[] filterOutUnknown(double[] containsUnknown) {
        return Arrays.stream(containsUnknown).filter(x -> x >= 0).toArray(); // filter out the unknown capacitance (-1)
    }

    /**
     * Solve the equivalent capacitance for the whole circuit and each branch.
     * Does not solve capacitance of any circuit.
     *
     * @throws InsufficientDataException if any cEqBranch (and therefore cEqCircuit if unknown) is unsolvable.
     */
    private void solveAllCEq() throws InsufficientDataException {
        if (this.cEqCircuitFound()) { //1a
            solveCEqBranches();
        } else { //1B
            if (this.cEqBranchesFound()) { //1BA
                this.cEqCircuit = CircuitLib.cEqSimpleParallel(this.cEqBranches);
                solveCEqBranches();
            } else { // 1BB
                solveCEqBranches();
                this.cEqCircuit = CircuitLib.cEqSimpleParallel(this.cEqBranches);
            }
        }
    }

    /**
     * Solve the equivalent capacitances of each branch of a parallel circuit.
     * <p>
     * A cEqBranch can be solved using the other cEqBranches and cEqCircuit
     * (as if the circuit was a parallel circuit with only one capacitor per branch),
     * or from the capacitances in the branch.
     *
     * @throws InsufficientDataException if a cEqBranch cannot be solved.
     */
    private void solveCEqBranches() throws InsufficientDataException {
        double[][] capacitances = this.getCapacitances();
        for (int row = 0; row < capacitances.length; row++) {
            if (this.cEqBranches[row] < 0) {
                if (countMissing(capacitances[row]) == 0) {// 1BA
                    this.cEqBranches[row] = CircuitLib.cEqSeries(capacitances[row]);
                } else if (cEqCircuitFound()) { // 1BB
                    if (countMissing(this.cEqBranches) == 1) { // 1BBA
                        this.cEqBranches[row] = CircuitLib.missingCapacitanceSimpleParallel(filterOutUnknown(this.cEqBranches), this.getCEqCircuit());
                        break; // this was the only missing one. We don't have to iterate through the rest.
                    } else { // 1BBB
                        // Find all other cEqBranches from their C's
                        for (int i = 0; i < capacitances.length; i++) { // 1bbb
                            if (i != row && this.cEqBranches[i] < 0) {
                                if (countMissing(capacitances[i]) == 0) { // 1bbba
                                    this.cEqBranches[i] = CircuitLib.cEqSeries(capacitances[i]);
                                } else { // 1bbbb
                                    throw new InsufficientDataException("WRITE A MESSAGE HERE");
                                }
                            }
                        }
                        // By now, all the other cEq branches are known. Solve this one.
                        this.cEqBranches[row] = CircuitLib.missingCapacitanceSimpleParallel(filterOutUnknown(this.cEqBranches), this.getCEqCircuit());
                        break; // we've already found the rest.
                    }
                } else { // 1BC
                    throw new InsufficientDataException("TODO: WRITE A MESSAGE HERE.");
                }
            }
        }
    }

    /**
     * Solve all missing capacitances of the circuit.
     * By this point, solveAllCEq (and, by extension, solveAllCEqBranches) has run,
     * so all equivalent capacitances have been solved.
     *
     * @throws InsufficientDataException if more than one capacitance in a branch is unknown (unsolvable).
     */
    private void solveAllC() throws InsufficientDataException { // 3
        double[][] capacitances = this.getCapacitances();
        for (int row = 0; row < capacitances.length; row++) {
            int numMissingC = countMissing(capacitances[row]);
            if (numMissingC == 1) {
                // find the missing C
                for (Component capacitor : this.capacitors[row]) {
                    if (capacitor.getValue() < 0) {
                        capacitor.setValue(
                                CircuitLib.missingCapacitanceSeries(
                                        filterOutUnknown(capacitances[row]),
                                        this.cEqBranches[row]
                                )
                        );
                    }
                }
            } else if (numMissingC > 1) {
                throw new InsufficientDataException("WRITE A MESSAGE HERE");
            }
        }
    }

    /**
     * Solve all missing values in the circuit.
     *
     * @throws InsufficientDataException if the circuit's missing values cannot be solved.
     */
    void justSolveTheWholeCircuitAlready() throws InsufficientDataException {
        solveAllCEq();
        // By now, all CEqBranches are known.
        solveAllC();
    }

    /**
     * Extract values from all capacitors in this.capacitors. Used in other methods.
     *
     * @return All the capacitance-values of this circuit.
     */
    private double[][] getCapacitances() {
        double[][] capacitances = new double[this.capacitors.length][];
        for (int row = 0; row < this.capacitors.length; row++) {
            Component[] capacitorRow = this.capacitors[row];
            double[] capacitanceRow = new double[capacitorRow.length];
            for (int col = 0; col < capacitorRow.length; col++) {
                double capacitance = capacitorRow[col].getValue();
                capacitanceRow[col] = capacitance;
            }
            capacitances[row] = capacitanceRow;
        }
        return capacitances;
    }

    /**
     * Get the equivalent capacitance (aka total capacitance).
     * If all the capacitors in the entire circuit were replaced by a single capacitor,
     * what should its capacitance be to ensure that there's no noticeable difference?
     *
     * @return The equivalent capacitance of the circuit.
     */
    double getCEqCircuit() {
        return this.cEqCircuit;
    }

    /**
     * Getter for the equivalent capacitance of each branch.
     * If all the capacitors in each branch were replaced by a single capacitor,
     * what should its capacitance be to ensure that there's no noticeable difference?
     *
     * @return the equivalent capacitance of each branch.
     */
    double[] getCEqBranches() {
        return this.cEqBranches;
    }

    /**
     * Getter for this.capacitors.
     * If necessary, solve for a missing capacitance.
     *
     * @return all capacitors in the circuits.
     */
    Component[][] getCapacitors() {
        // iterate trhough capacitances
        return this.capacitors;
    }

    /**
     * Create a String representation of a circuit object (our file format)
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        Component[][] solvedCapacitors = this.getCapacitors();
        // Each branch automatically adds two lines: one for a hyphen and one for cEqBranch.
        // Then there's the last hyphen, Units, emf, and cEqCircuit.
        int lineLength = solvedCapacitors.length * 2 + 4; // does not include lines for capacitors.
        for (Component[] line : solvedCapacitors) {
            lineLength += line.length;
        }
        String[] allLines = new String[lineLength];
        allLines[0] = "Units " + this.units[0] + " " + this.units[1];
        allLines[1] = this.powerSupply.toString();
        allLines[2] = "-";
        int currentLine = 3;
        double[] cEqBranches = this.getCEqBranches();
        for (int i = 0; i < solvedCapacitors.length; i++) {
            Component[] branch = solvedCapacitors[i];
            for (Component capacitor : branch) {
                allLines[currentLine] = capacitor.toString();
                currentLine++;
            }
            allLines[currentLine] = "CEq " + cEqBranches[i];
            allLines[currentLine + 1] = "-";
            currentLine += 2;
        }
        allLines[allLines.length - 1] = "cEq " + this.getCEqCircuit();
        // if we end up concatenating arrays a lot, we should make a method for it.
        return String.join("\n", allLines);
    }

}

/**
 * An exception to throw if a circuit is unsolvable due to a lack of information.
 */
class InsufficientDataException extends Exception {
    InsufficientDataException(String messageGiven) {
        super(messageGiven);
    }

    public String toString() {
        return ("InsufficientDataException Occurred: " + this.getMessage());
    }
}