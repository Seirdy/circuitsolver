isOneDone() = false
2done = false

1. Find cEqCircuit. If:
	1a) cEqCircuit is already known:
		1a1. 1done = true
		1a2. Move to 2.
	1b) cEqCircuit is not alreay known
		1ba) All cEqBranches are known
			1ba1. Solve for cEq using cEqBranches
			1ba2. 1done = true
			1ba3. Move to 2.
		1bb) Not all cEqBranches are known:
			1bb1. Do 2.
			1bb1. Go to 1ba.
2. Find cEqBranches (or throw an error). For each branch:
	1a) cEqBranch is known:
		1aa) This is not the last branch: keep going.
		1ab) This is the last branch:
			1ab1. 2done = true
			1ab2. Move to 3
	1b) cEqBranch is unknown:
		1ba) All C's in this branch are known: Solve.
		1bb) 1done == true
			1bba) All other cEqBranches are known
				1bba1. Solve using other cEqBranches and cEqCircuit
				1bba2. 2done = true
				1bba3. Move to 3.
			1bbb) Not all other cEqBranches are known
				1bbba) All other branches have all their C's:
					1bbba1. Solve them
					1bbba2. Do 1bba)
				1bbbb) Not all other branches have their C's: Throw an error.
		1bc) 1done == false: throw an error.
3. Find all C's. For each branch:
	NOTE: If cEqBranch is unknown, WE HAVE A BUG.
	1a) All C's are known
	1b) More than one C is unknown OR there is only one C and it is unknown: ERROR
	1c) One C is unknown and the rest are known: Solve.
