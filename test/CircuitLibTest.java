import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test that CircuitLib does physics correctly.
 */
class CircuitLibTest {

    /**
     * Does cEqSeries() correctly find CEq for capacitors in series?
     */
    @Test
    void TestCEqSeries() {
        double[] capacitors = {4, 6, 3, 7, 18};
        assertEquals(252.0 / 239.0, CircuitLib.cEqSeries(capacitors));
    }

    /**
     * Does MissingCapacitance() correctly find a missing capacitance?
     */
    @Test
    void TestMissingCapacitanceSeries() {
        double[] capacitors = {4, 6, 3, 18};
        double CEq = 252.0 / 239.0;
        double expected = 7;
        double actual = CircuitLib.missingCapacitanceSeries(capacitors, CEq);
        assertEquals(expected, actual, 1e-3); // Thanks, floating-point errors! ugh.
    }
}