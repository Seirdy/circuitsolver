import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test all parsers in the Parsing library.
 */
class ParsingTest {

    @Test
    void parseInputParsesInput() {
        // Make an example file. Each line is an element in a 1D array.
        String[] exampleFile1 = {"Units V uF", "emf BAT 9", "Capacitor C1 1", "Capacitor C2 -1", "Capacitor C3 2", "CEq 0.4"};
        // no reason why two capacitors can't have the same name at this stage of the program. Circuit has four capacitors.
        String[] exampleFile2 = {"Units V uF", "emf BAT 16", "Capacitor C1 3", "Capacitor C2 5", "Capacitor C3 -1", "Capacitor C3 7", "CEq 2"};
        String[] exampleFile3 = {"Units V uF", "emf BAT 6", "Capacitor C1 -1", "Capacitor C2 3", "CEq 4"}; // Circuit has two capacitors.
        // Make the expected output.
        String[][] expectedOutput1 = {{"Units", "V", "uF"}, {"emf", "BAT", "9"}, {"Capacitor", "C1", "1"}, {"Capacitor", "C2", "-1"}, {"Capacitor", "C3", "2"}, {"CEq", "0.4"}};
        String[][] expectedOutput2 = {{"Units", "V", "uF"}, {"emf", "BAT", "16"}, {"Capacitor", "C1", "3"}, {"Capacitor", "C2", "5"}, {"Capacitor", "C3", "-1"}, {"Capacitor", "C3", "7"}, {"CEq", "2"}};
        String[][] expectedOutput3 = {{"Units", "V", "uF"}, {"emf", "BAT", "6"}, {"Capacitor", "C1", "-1"}, {"Capacitor", "C2", "3"}, {"CEq", "4"}};
        // The actual outputs.
        String[][] actualOutput1 = Parsing.parseCircuit(exampleFile1);
        String[][] actualOutput2 = Parsing.parseCircuit(exampleFile2);
        String[][] actualOutput3 = Parsing.parseCircuit(exampleFile3);
        // Converting to string and then comparing because for some reason this test isn't passing even
        // though the arrays have the same elements. TODO (V2): Do a for-loop for each element to test equality.
        assertEquals(Arrays.deepToString(expectedOutput1), Arrays.deepToString(actualOutput1));
        assertEquals(Arrays.deepToString(expectedOutput2), Arrays.deepToString(actualOutput2));
        assertEquals(Arrays.deepToString(expectedOutput3), Arrays.deepToString(actualOutput3));
    }

    @Test
    void parseComponentsParsesComponents() {
        // Make a list of capacitors
        // TODO (V1) (Garrett): Make exampleUnparsedBatteries 1, 2, 3.
        String[][] unparsedCapacitors = {{"Capacitor", "C1", "12.5"}, {"Capacitor", "C2", "8"}, {"Capacitor", "C3", "5"}};
        Component expectedCapacitor0 = new Component("C1", "Capacitor", 12.5);
        Component expectedCapacitor1 = new Component("C2", "Capacitor", 8);
        Component expectedCapacitor2 = new Component("C3", "Capacitor", 5);
        Component[] parsedCapacitors = Parsing.parseComponents(unparsedCapacitors);
        /*TODO: figure out how to override .equals() with param Object.*/
        assert parsedCapacitors[0].circuitEquals(expectedCapacitor0);
        assert parsedCapacitors[1].circuitEquals(expectedCapacitor1);
        assert parsedCapacitors[2].circuitEquals(expectedCapacitor2);
        // also test getters and setters.
    }
}