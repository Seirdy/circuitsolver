import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests that Circuit's getters/setters work properly.
 * They need to calculate the correct values and throw exceptions when necessary.
 */
class CircuitTest {

    @Test
    void circuit1GetsSolved() throws InsufficientDataException {

        //testFile1Incomplete1 -> testFile1Complete
        Component C1 = new Component("C1", "Capacitor", -1);
        Component C2 = new Component("C2", "Capacitor", 6.3);
        Component C3 = new Component("C3", "Capacitor", 9.0);
        Component C4 = new Component("C4", "Capacitor", 8.2);
        Component C5 = new Component("C5", "Capacitor", 2.03);
        Component C6 = new Component("C6", "Capacitor", 9.2);

        Component powerSupply = new Component("BAT", "Battery", 10.0);
        String[] units = {"V", "uF"};
        double cEqCircuit = 3.612721529;
        double[] cEqBranches = {2.230088496, 1.382633033};

        Component[][] capacitors = {{C1, C2, C3}, {C4, C5, C6}};

        Circuit incompleteCircuit1 = new Circuit(capacitors, powerSupply, units, cEqCircuit, cEqBranches);
        incompleteCircuit1.justSolveTheWholeCircuitAlready();

        assertEquals(incompleteCircuit1.getCapacitors()[0][0].getValue(), 5.6, 1e-3);
        assertEquals(incompleteCircuit1.getCapacitors()[0][1].getValue(), 6.3, 1e-3);
        assertEquals(incompleteCircuit1.getCapacitors()[0][2].getValue(), 9.0, 1e-3);
        assertEquals(incompleteCircuit1.getCapacitors()[1][0].getValue(), 8.2, 1e-3);
        assertEquals(incompleteCircuit1.getCapacitors()[1][1].getValue(), 2.03, 1e-3);
        assertEquals(incompleteCircuit1.getCapacitors()[1][2].getValue(), 9.2, 1e-3);
        assertEquals(incompleteCircuit1.getCEqBranches()[0], 2.230088496, 1e-3);
        assertEquals(incompleteCircuit1.getCEqBranches()[1], 1.382633033, 1e-3);
        assertEquals(incompleteCircuit1.getCEqCircuit(), 3.612721529, 1e-3);


    }


    @Test
    void circuit2GetsSolved() throws InsufficientDataException {
        //testFile2Incomplete1 -> testFile2Complete
        Component C1 = new Component("C1", "Capacitor", 4);
        Component C2 = new Component("C2", "Capacitor", -1);
        Component C3 = new Component("C3", "Capacitor", 2);
        Component C4 = new Component("C4", "Capacitor", 2);
        Component C5 = new Component("C5", "Capacitor", 2);


        Component powerSupply = new Component("BAT", "Battery", 5.0);
        String[] units = {"V", "uF"};
        double cEqCircuit = 2;
        double[] cEqBranches = {-1, 1};

        Component[][] capacitors = {{C1, C2, C3}, {C4, C5}};

        Circuit incompleteCircuit2 = new Circuit(capacitors, powerSupply, units, cEqCircuit, cEqBranches);
        incompleteCircuit2.justSolveTheWholeCircuitAlready();

        assertEquals(incompleteCircuit2.getCapacitors()[0][0].getValue(), 4, 1e-3);
        assertEquals(incompleteCircuit2.getCapacitors()[0][1].getValue(), 4, 1e-3);
        assertEquals(incompleteCircuit2.getCapacitors()[0][2].getValue(), 2, 1e-3);
        assertEquals(incompleteCircuit2.getCapacitors()[1][0].getValue(), 2, 1e-3);
        assertEquals(incompleteCircuit2.getCapacitors()[1][1].getValue(), 2, 1e-3);
        assertEquals(incompleteCircuit2.getCEqBranches()[0], 1, 1e-3);
        assertEquals(incompleteCircuit2.getCEqBranches()[1], 1, 1e-3);
        assertEquals(incompleteCircuit2.getCEqCircuit(), 2, 1e-3);
    }

    @Test
    void circuit3GetsSolved() throws InsufficientDataException {
        //testFile3Incomplete1 -> testFile3Complete
        Component C1 = new Component("C1", "Capacitor", -1);
        Component C2 = new Component("C2", "Capacitor", 2);
        Component C3 = new Component("C3", "Capacitor", 4);
        Component C4 = new Component("C4", "Capacitor", -1);
        Component C5 = new Component("C5", "Capacitor", 8);
        Component C6 = new Component("C5", "Capacitor", 8);


        Component powerSupply = new Component("BAT", "Battery", 10.0);
        String[] units = {"V", "uF"};
        double cEqCircuit = 7;
        double[] cEqBranches = {-1, 2, 4};

        Component[][] capacitors = {{C1, C2}, {C3, C4}, {C5, C6}};

        Circuit incompleteCircuit3 = new Circuit(capacitors, powerSupply, units, cEqCircuit, cEqBranches);
        incompleteCircuit3.justSolveTheWholeCircuitAlready();

        assertEquals(incompleteCircuit3.getCapacitors()[0][0].getValue(), 2, 1e-3);
        assertEquals(incompleteCircuit3.getCapacitors()[0][1].getValue(), 2, 1e-3);
        assertEquals(incompleteCircuit3.getCapacitors()[1][0].getValue(), 4, 1e-3);
        assertEquals(incompleteCircuit3.getCapacitors()[1][1].getValue(), 4, 1e-3);
        assertEquals(incompleteCircuit3.getCapacitors()[2][0].getValue(), 8, 1e-3);
        assertEquals(incompleteCircuit3.getCapacitors()[2][1].getValue(), 8, 1e-3);
        assertEquals(incompleteCircuit3.getCEqBranches()[0], 1, 1e-3);
        assertEquals(incompleteCircuit3.getCEqBranches()[1], 2, 1e-3);
        assertEquals(incompleteCircuit3.getCEqBranches()[2], 4, 1e-3);
        assertEquals(incompleteCircuit3.getCEqCircuit(), 7, 1e-3);

    }
}