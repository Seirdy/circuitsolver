# CircuitSolver

Solve simple circuits typically used in an introductory physics class. Final project for CS II at Lewis and Clark College.

[Documentation](https://gitlab.com/Seirdy/circuitsolver/tree/master/doc) (Javadoc format) is in the Doc/ directory.

You can download and run a JAR file from the [Tags](https://gitlab.com/Seirdy/circuitsolver/tags) page. It requires an up-to-date version of the Java Runtime; it must be able to recognize a class file of version 54.

## Terminology

"cEqCircuit": The equivalent capacitance, also known as the total capacitance.
"emf": Electrical potential difference ("voltage") supplied to the circuit.

## UML diagram

![UML Diagram](https://gitlab.com/Seirdy/circuitsolver/raw/master/images/V1_UML.svg)

## Version 2

We have finished making CircuitSolver able to handle circuits in parallel, and have written tests for Circuit.java.

### Example usage

CircuitSolver is meant to be used as a command-line utility that reads a circuit with missing values from standard input and outputs a solved circuit standard output.

```
$ ls
CircuitSolver.jar@  unsolvedCircuit.txt
$ java -jar CircuitSolver.jar < unsolvedCircuit.txt > solvedCircuit.txt
$ ls
CircuitSolver.jar@  unsolvedCircuit.txt  solvedCircuit.txt
```

### The file format

Hyphens represent branches in parallel circuits. Each branch and circuit has an equivalent capacitance.

A sample parallel circuit file:

```
Units V uF
emf BAT 9.0
-
Capacitor C1 4.0
Capacitor C2 7.0
Capacitor C3 6.0
Capacitor C4 -1
Capacitor C5 18.0
CEq -1
-
Capacitor C6 3.2
Capacitor C7 9.0
CEq -1
-
CEq 3.415049043144249
```

A sample series circuit file:

```
Units V uF
emf BAT 9.0
-
Capacitor C1 4.0
Capacitor C2 7.0
Capacitor C3 6.0
Capacitor C4 3
Capacitor C5 18.0
CEq -1
-
CEq -1

```

More sample circuit files can be found in the [sampleCircuitFiles directory](./sampleCircuitFiles), with both complete and incomplete (but solvable) circuits.

## Version 1

We have made full implementations of all classes in this UML diagram except CircuitTest; CircuitTest contains empty methods with javadocs describing all the things we plan to test. This is partly because we plan to alter part of the behavior of Circuit in Version 2.

### Usage

CircuitSolver.jar reads an incomplete circuit from standard input. If the incomplete circuit is solvable, it prints a solved circuit to standard output. If the incomplete circuit is unsolvable, it throws an error.

#### Example usage

```
$ ls
CircuitSolver.jar@  unsolvedCircuit.txt
$ cat unsolvedCircuit.txt
Units V uF
emf BAT 9
Capacitor C1 4
Capacitor C2 7
Capacitor C3 6
Capacitor C4 -1
Capacitor C5 18
cEqCircuit 1.054393305439330543933
$ java -jar CircuitSolver.jar < unsolvedCircuit.txt > solvedCircuit.txt
$ cat solvedCircuit.txt
Units V uF
emf BAT 9.0
Capacitor C1 4.0
Capacitor C2 7.0
Capacitor C3 6.0
Capacitor C4 2.9999999999999996
Capacitor C5 18.0
cEqCircuit 1.0543933054393306
```

## Authors

POGIL Team: Anything/Vim

Members: Rohan Kumar, Arundhati Suresh, Sam Li, Garrett Bosse
